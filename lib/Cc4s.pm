# vim:filetype=perl6
use v6;

unit module Cc4s;

enum PlaceHolder ( <_TENSOR_NAME_>
                 , <_PATH_TO_VERTEX_FILE_>
                 , <_PATH_TO_XYZ_FILE_>
                 , <_BASIS_SET_>
                 , <_MOVEC_FILE_>
                 )
                 ;

#| A cc4s boolean type, either 0 or 1
subset Boolean   of Int  where * ∈ (0, 1);
subset DataType  of Str  where * (elem) < RealTensor ComplexTensor >;
subset StrInput  of List where (Str, Str);
# write DataType to differentiate DataInput from StrInput
subset DataInput of List where (Str, Str, DataType);
subset IntInput  of List where (Str, Int);
subset BoolInput of List where (Str, Boolean);
subset RealInput of List where (Str, Num) | (Str, Rat);
subset NoInput   of List where ();
subset Input     of List where StrInput  | DataInput | IntInput
                             | RealInput | BoolInput | NoInput
                             ;

multi to-string(NoInput $a) of Str is export {""}
multi to-string(StrInput $a) of Str is export {qq!({$a.head} "{$a.tail}")!}
multi to-string(RealInput $a) of Str is export {
  "({$a.head} {$a.tail ~~ /e/ ?? $a.tail.fmt("%.20f") !! $a.tail })"
}
multi to-string(DataInput $a) of Str is export {
  ($a.head ~~ $a[1]) ?? $a.head !! "({$a.head} {$a[1]})"
}
multi to-string(Input $a) of Str is export {"($a)"}

sub mk-tensor
  ( Str $tsr
  , Str $name = $tsr
  , DataType $dt = <RealTensor>
  --> DataInput
  ) is export {
    ($tsr, $name, $dt)
}

class Algorithm is export {
  has Str $.name;
  has Input @.inputs;
  has Input @.outputs;
  has Signature $.signature;
  method Str {qq!
$.name [
  { (@.inputs  ==> map &to-string ==> grep /^^ . + $$/ ).join: "\n  " }
] [
  { (@.outputs ==> map &to-string ==> grep /^^ . + $$/ ).join: "\n  " }
].
!.subst: /^^ \s+ $$/, ""}

  method add-inputs (@i) of Algorithm { @.inputs.append:
                                          Array[Input].new(@i); self }
  method add-outputs (@o) of Algorithm { @.outputs.append:
                                          Array[Input].new(@o); self }

  method help {
    qq!
    The signature of the $!name is

    { $!signature.gist.subst: /\,/, "\n", :g }

    !
  }
}

multi sub prefix:<%!>(Algorithm $a) of Str is export { "" }

subset CoulombIntegral of Str
  where * ∈ (([X~] $_ xx 4) X~ <CoulombIntegrals> given < H P >);

sub real-tensors-if-given (*%tsrs) of Array[DataInput] is export {
  Array[DataInput].new: (
    (.keys[0], (.values[0] eq True) ?? .keys[0] !! .values[0], <RealTensor>)
    for %tsrs ==> grep {so .values[0]}
  )
}

sub data-input ($name, $alias = $name, DataType $type=<RealTensor>)
    of DataInput is export {
  ($name, $alias, $type)
}

#| Template function for most tensor input/output functions in cc4s
sub TensorIO
  ( Str :$name
  , Str :$Data = ~_TENSOR_NAME_
  , Str :$file = ""
  , Bool :$bin = False
  , DataType :dtype($dtype) = <RealTensor>
  ) of Algorithm
  {
  Algorithm.new:
    :name($name)
    :signature(&?ROUTINE.signature)
    :inputs( (<Data>, $Data, $dtype)
           , $file ?? (<file>, $file) !! ()
           , $bin ?? (<mode>, <binary>) !! ()
           )
}

our &TensorReader is export = &TensorIO.assuming(:name<TensorReader>);
our &TensorWriter is export = &TensorIO.assuming(:name<TensorWriter>);
our &ComplexTensorReader is export
    = &TensorIO.assuming(:name<ComplexTensorReader>,
                         :dtype<ComplexTensor>);
our &ComplexTensorWriter is export
    = &TensorIO.assuming(:name<ComplexTensorWriter>,
                         :dtype<ComplexTensor>);

#| CoulombVertexReader
sub CoulombVertexReader
  ( Str :file($file) = ~_PATH_TO_VERTEX_FILE_
  , Str :CoulombVertex($vertex) = <CoulombVertex>
  , Str :HoleEigenEnergies($h) = <HoleEigenEnergies>
  , Str :ParticleEigenEnergies($p) = <ParticleEigenEnergies>
  ) is export
  {

  Algorithm.new:
    :name(&?ROUTINE.name)
    :signature(&?ROUTINE.signature)
    :inputs( (<file>, $file), )
    :outputs( (<CoulombVertex>, $vertex, <RealTensor>)
            , (<HoleEigenEnergies>, $h, <RealTensor>)
            , (<ParticleEigenEnergies>, $p, <RealTensor>)
            )

}

sub FockMatrixFromCoulombIntegrals
  ( Str :$HHMatrix = <HHMatrix>
  , Str :$HHHHCoulombIntegrals = <HHHHCoulombIntegrals>
  , Str :$PPMatrix = <PPMatrix>
  , Str :$PHPHCoulombIntegrals = <PHPHCoulombIntegrals>
  , Str :$PHHPCoulombIntegrals = <PHHPCoulombIntegrals>
  , Str :$PHMatrix = <PHMatrix>
  , Str :$PHHHCoulombIntegrals = <PHHHCoulombIntegrals>
  # outputs
  , Str :$HoleEigenEnergies = <HoleEigenEnergies>
  , Str :$ParticleEigenEnergies = <ParticleEigenEnergies>
  , Str :$HHFockMatrix = <HHFockMatrix>
  , Str :$PHFockMatrix = <PHFockMatrix>
  , Str :$HPFockMatrix = <HPFockMatrix>
  , Str :$PPFockMatrix = <PPFockMatrix>
  ) of Algorithm is export
  {
  Algorithm.new:
    :name(&?ROUTINE.name)
    :signature(&?ROUTINE.signature)
    :inputs( |$_ given real-tensors-if-given :$HHMatrix
                                             :$HHHHCoulombIntegrals
                                             :$PPMatrix
                                             :$PHPHCoulombIntegrals
                                             :$PHHPCoulombIntegrals
                                             :$PHMatrix
                                             :$PHHHCoulombIntegrals)
    :outputs( |$_ given real-tensors-if-given :$HoleEigenEnergies
                                              :$ParticleEigenEnergies
                                              :$HHFockMatrix
                                              :$PHFockMatrix
                                              :$HPFockMatrix
                                              :$PPFockMatrix)
}

# Convert a hash of { :PPHHCoulombIntegrals, :WHATEVER }
# into a list of DataInput of CoulombIntegrals ready to be fed into a cc4s algo
sub cints-hash-to-input-list (%ints) of Array[DataInput] is export {
   real-tensors-if-given |$_
     given hash %ints.grep: {.keys[0] ~~ CoulombIntegral}
}

#| CoulombIntegralsFromVertex
sub CoulombIntegralsFromVertex
  ( Str :CoulombVertex($v) = <CoulombVertex>
  , Str :HoleEigenEnergies($h) = <HoleEigenEnergies>
  , Str :ParticleEigenEnergies($p) = <ParticleEigenEnergies>
  #| This should be any combination of :HHHHCoulombIntegrals, ...
  , *%ints
  ) is export
  {
  Algorithm.new:
    :name(&?ROUTINE.name)
    :signature(&?ROUTINE.signature)
    :inputs( (<CoulombVertex>, $v, <RealTensor>)
           , (<HoleEigenEnergies>, $h, <RealTensor>)
           , (<ParticleEigenEnergies>, $p, <RealTensor>)
           )
    :outputs(cints-hash-to-input-list %ints)
}

#| Mp2EnergyFromCoulombIntegrals
sub Mp2EnergyFromCoulombIntegrals
  ( Str :PPHHCoulombIntegrals($pphh) = <PPHHCoulombIntegrals>
  , Str :HoleEigenEnergies($h) = <HoleEigenEnergies>
  , Str :ParticleEigenEnergies($p) = <ParticleEigenEnergies>
  , Str :Mp2Energy($energy) = <Mp2Energy>
  , Str :Mp2DoublesAmplitudes($amplitudes) = <Mp2DoublesAmplitudes>
  ) is export {

  Algorithm.new:
    :name(&?ROUTINE.name)
    :signature(&?ROUTINE.signature)
    :inputs( (<HoleEigenEnergies>, $h, <RealTensor>)
           , (<ParticleEigenEnergies>, $p, <RealTensor>)
           , (<PPHHCoulombIntegrals>, $pphh, <RealTensor>)
           )
    :outputs( (<Mp2Energy>, $energy, <RealTensor>)
            , (<Mp2DoublesAmplitudes>, $amplitudes, <RealTensor>)
            )

}

#| CcsdEnergyFromCoulombIntegrals
sub CcsdEnergyFromCoulombIntegrals
  ( Str :PPHHCoulombIntegrals($pphh) = <PPHHCoulombIntegrals>
  , Str :PHPHCoulombIntegrals($phph) = <PHPHCoulombIntegrals>
  , Str :HHHHCoulombIntegrals($hhhh) = <HHHHCoulombIntegrals>
  , Str :HHHPCoulombIntegrals($hhhp) = <HHHPCoulombIntegrals>
  , Str :CoulombVertex($vertex) = <CoulombVertex>
  , Str :HoleEigenEnergies($h) = <HoleEigenEnergies>
  , Str :ParticleEigenEnergies($p) = <ParticleEigenEnergies>
  , Str :initialDoublesAmplitudes($i-tabij) = ""
  , Str :initialSinglesAmplitudes($i-tai) = ""
  , Boolean :distinguishable($dist) = 0
  , Boolean :PPL($ppl) = 0
  , Int :integralsSliceSize($sliceSize) = 0
  , Str :mixer($mixer) = ""
  , Real :mixingRatio($mixRat) = -1
  , Int :maxResidua($maxRes) = 0
  , Int :maxIterations($maxIter) = 0
  , Real :amplitudesConvergence($ampConv) = -1
  , Real :energyConvergence($enConv) = -1
  , Str :CcsdEnergy($energy) = <CcsdEnergy>
  , Str :CcsdSinglesAmplitudes($tai) = ""
  , Str :CcsdDoublesAmplitudes($tabij) = ""
  ) is export {

  Algorithm.new:
    :name(&?ROUTINE.name)
    :signature(&?ROUTINE.signature)
    :inputs( (<CoulombVertex>, $vertex, <RealTensor>)
           , (<HoleEigenEnergies>, $h, <RealTensor>)
           , (<ParticleEigenEnergies>, $p, <RealTensor>)
           , (<PPHHCoulombIntegrals>, $pphh, <RealTensor>)
           , (<PHPHCoulombIntegrals>, $phph, <RealTensor>)
           , (<HHHHCoulombIntegrals>, $hhhh, <RealTensor>)
           , (<HHHPCoulombIntegrals>, $hhhp, <RealTensor>)
           , $i-tabij ?? (<initialDoublesAmplitudes>, $i-tabij, <RealTensor>) !! ()
           , $i-tai ?? (<initialSinglesAmplitudes>, $i-tai, <RealTensor>) !! ()
           , $dist ?? (<distinguishable>, $dist)  !! ()
           , $ppl ?? (<PPL>, $ppl) !! ()
           , $sliceSize ?? (<integralsSliceSize>, $sliceSize) !! ()
           , $mixer ?? (<mixer>, $mixer) !! ()
           , ($mixRat > 0) ?? (<mixingRatio>, $mixRat) !! ()
           , $maxRes ?? (<maxResidua>, $maxRes) !! ()
           , $maxIter ?? (<maxIterations>, $maxIter) !! ()
           , ($ampConv > 0) ?? (<amplitudesConvergence> , $ampConv) !! ()
           , ($enConv > 0) ?? (<energyConvergence> , $enConv) !! ()
           )
    :outputs( (<CcsdEnergy>, $energy, <RealTensor>)
            , $tai ?? (<CcsdSinglesAmplitudes>, $tai, <RealTensor>) !! ()
            , $tabij ?? (<CcsdDoublesAmplitudes>, $tabij, <RealTensor>) !! ()
            )

}

#| CcsdEnergyFromCoulombIntegralsReference
sub CcsdEnergyFromCoulombIntegralsReference
  ( Str :PPHHCoulombIntegrals($pphh) = <PPHHCoulombIntegrals>
  , Str :PHPHCoulombIntegrals($phph) = <PHPHCoulombIntegrals>
  , Str :HHHHCoulombIntegrals($hhhh) = <HHHHCoulombIntegrals>
  , Str :HHHPCoulombIntegrals($hhhp) = <HHHPCoulombIntegrals>
  , Str :PPPHCoulombIntegrals($ppph) = <PPPHCoulombIntegrals>
  , Str :PPPPCoulombIntegrals($pppp) = <PPPPCoulombIntegrals>
  , Str :HoleEigenEnergies($h) = <HoleEigenEnergies>
  , Str :ParticleEigenEnergies($p) = <ParticleEigenEnergies>
  , Str :initialDoublesAmplitudes($i-tabij) = ""
  , Str :initialSinglesAmplitudes($i-tai) = ""
  , Boolean :onlyPPL($ppl) = 0
  , Str :mixer($mixer) = ""
  , Real :mixingRatio($mixRat) = -1
  , Int :maxResidua($maxRes) = 0
  , Int :maxIterations($maxIter) = 0
  , Real :amplitudesConvergence($ampConv) = -1
  , Real :energyConvergence($enConv) = -1
  , Str :CcsdEnergy($energy) = <CcsdEnergy>
  , Str :CcsdSinglesAmplitudes($tai) = ""
  , Str :CcsdDoublesAmplitudes($tabij) = ""
  , Str :PairEnergy($pairEnergy) = ""
  , Boolean :Mp2Only($mp2) = 0
  ) is export {

  Algorithm.new:
    :name(&?ROUTINE.name)
    :signature(&?ROUTINE.signature)
    :inputs( (<HoleEigenEnergies>, $h, <RealTensor>)
           , (<ParticleEigenEnergies>, $p, <RealTensor>)
           , (<PPHHCoulombIntegrals>, $pphh, <RealTensor>)
           , $mp2 ?? () !! (<PHPHCoulombIntegrals>, $phph, <RealTensor>)
           , $mp2 ?? () !! (<HHHHCoulombIntegrals>, $hhhh, <RealTensor>)
           , $mp2 ?? () !! (<HHHPCoulombIntegrals>, $hhhp, <RealTensor>)
           , $mp2 ?? () !! (<PPPHCoulombIntegrals>, $ppph, <RealTensor>)
           , $mp2 ?? () !! (<PPPPCoulombIntegrals>, $pppp, <RealTensor>)
           , $i-tabij ?? (<initialDoublesAmplitudes>, $i-tabij, <RealTensor>) !! ()
           , $i-tai ?? (<initialSinglesAmplitudes>, $i-tai, <RealTensor>) !! ()
           , $ppl ?? (<onlyPPL>, $ppl) !! ()
           , $mixer ?? (<mixer>, $mixer) !! ()
           , ($mixRat > 0) ?? (<mixingRatio>, $mixRat) !! ()
           , $maxRes ?? (<maxResidua>, $maxRes) !! ()
           , $maxIter ?? (<maxIterations>, $maxIter) !! ()
           , ($ampConv > 0) ?? (<amplitudesConvergence> , $ampConv) !! ()
           , ($enConv > 0) ?? (<energyConvergence> , $enConv) !! ()
           )
    :outputs( (<CcsdEnergy>, $energy, <RealTensor>)
            , $tai ?? (<CcsdSinglesAmplitudes>, $tai, <RealTensor>) !! ()
            , $tabij ?? (<CcsdDoublesAmplitudes>, $tabij, <RealTensor>) !! ()
            , $pairEnergy ?? (<PairEnergy>, $pairEnergy, <RealTensor>) !! ()
            )

}

#| OneBodyFromGaussian
sub OneBodyFromGaussian
  ( Str :$kernel is required where * ∈ < nuclear overlap kinetic >
  , Str :$Out is required
  , Str :$xyzStructureFile = ~_PATH_TO_XYZ_FILE_
  , Str :$basisSet = ~_BASIS_SET_
  ) of Algorithm is export
  {
  Algorithm.new:
    :name(&?ROUTINE.name)
    :signature(&?ROUTINE.signature)
    :inputs( :$xyzStructureFile.kv.list
           , :$basisSet.kv.list
           , :$kernel.kv.list
           )
    :outputs( (<Out>, $Out, <RealTensor>), )
}

#| TensorSum
sub TensorSum
  ( Str :$AIndex!
  , Str :$BIndex!
  , Str :$ResultIndex!
  , Real :$AFactor = 1.0
  , Real :$BFactor = 1.0
  , Str :$A = ~_TENSOR_NAME_
  , Str :$B = ~_TENSOR_NAME_
  , Str :$Result = ~_TENSOR_NAME_
  ) of Algorithm is export
  {
  Algorithm.new:
    :name(&?ROUTINE.name)
    :signature(&?ROUTINE.signature)
    :inputs( :$AIndex.kv.list
           , :$BIndex.kv.list
           , :$ResultIndex.kv.list
           , :$AFactor.kv.list
           , :$BFactor.kv.list
           , (<A>, $A, <RealTensor>)
           , (<B>, $B, <RealTensor>)
           )
   :outputs( (<Result>, $Result, <RealTensor>), )
}

#| CoulombIntegralsFromGaussian
sub CoulombIntegralsFromGaussian
  ( Str :xyzStructureFile($structure) = ~_PATH_TO_XYZ_FILE_
  , Str :basisSet($basis) = ~_BASIS_SET_
  , Str :CoulombIntegrals($cs) = <CoulombIntegrals>
  , Str :HoleEigenEnergies($h) = ""
  , Str :OrbitalCoefficients($oc) = ""
  , Str :PPHHCoulombIntegrals($pphh) = ""
  , Str :HHHHCoulombIntegrals($hhhh) = ""
  , Str :Spins($s) = ""
  , Str :kernel($kernel) where * ∈ < coulomb delta > = <coulomb>
  , Str :shellDistribution($dist) = <roundRobin>
  , Boolean :chemistNotation($c-n) = 1
  ) of Algorithm is export
  {

  Algorithm.new:
    :name(&?ROUTINE.name)
    :signature(&?ROUTINE.signature)
    :inputs( (<xyzStructureFile>, $structure)
           , (<basisSet>, $basis)
           , (<kernel>, $kernel)
           , (<chemistNotation>, $c-n)
           , (<shellDistribution>, $dist)
           , $h ?? (<HoleEigenEnergies>, $h, <RealTensor>) !! ()
           , $oc ?? (<OrbitalCoefficients>, $oc, <RealTensor>) !! ()
           , $s ?? (<Spins>, $s, <RealTensor>) !! ()
           )
    :outputs(
             $h ?? (<PPHHCoulombIntegrals>, $pphh, <RealTensor>) !! ()
           , $h ?? (<HHHHCoulombIntegrals>, $hhhh, <RealTensor>)
                !! (<CoulombIntegrals>, $cs, <RealTensor>)
           )

}

#| HartreeFockFromGaussian
sub HartreeFockFromGaussian
  ( Str :$xyzStructureFile = ~_PATH_TO_XYZ_FILE_
  , Str :$basisSet = ~_BASIS_SET_
  , Real :$energyDifference = 1e-6
  , Int :$maxIterations = 50
  , Int :numberOfElectrons($nelec) where * > -2 = -1
  # outputs
  , Str :$CoreHamiltonian = ""
  , Str :$HartreeFockEnergy = <HartreeFockEnergy>
  , Str :$HoleEigenEnergies = <HoleEigenEnergies>
  , Str :$OrbitalCoefficients = <OrbitalCoefficients>
  , Str :$OverlapMatrix = ""
  , Str :$ParticleEigenEnergies = <ParticleEigenEnergies>
  ) of Algorithm is export
  {
  Algorithm.new:
    :name(&?ROUTINE.name)
    :signature(&?ROUTINE.signature)
    :inputs( :$xyzStructureFile.kv.list
           , :$basisSet.kv.list
           , :$maxIterations.kv.list
           , :$energyDifference.kv.list
           , $nelec eq -1 ?? () !! (<numberOfElectrons>, $nelec)
           )
    :outputs( |$_ given real-tensors-if-given :$CoreHamiltonian
                                              :$OrbitalCoefficients
                                              :$OverlapMatrix
                                              :$HartreeFockEnergy
                                              :$HoleEigenEnergies
                                              :$ParticleEigenEnergies)
}

#| HartreeFockFromCoulombIntegrals
sub HartreeFockFromCoulombIntegrals
  ( Int :$No is required
  , Str :$CoulombIntegrals = <CoulombIntegrals>
  , Str :$OverlapMatrix = <OverlapMatrix>
  , Str :$h = <h>
  , Real :$energyDifference = 0.000001
  , Int :$maxIterations = 50
  # outputs
  , Str :$HartreeFockEnergy = <HartreeFockEnergy>
  , Str :$HoleEigenEnergies = <HoleEigenEnergies>
  , Str :$OrbitalCoefficients = <OrbitalCoefficients>
  , Str :$ParticleEigenEnergies = <ParticleEigenEnergies>
  ) of Algorithm is export
  {
  Algorithm.new:
    :name(&?ROUTINE.name)
    :signature(&?ROUTINE.signature)
    :inputs( :$maxIterations.kv.list
           , :$No.kv.list
           , :$energyDifference.kv.list
           , |$_ given real-tensors-if-given :$CoulombIntegrals
                                           :$OverlapMatrix
                                           :$h)
    :outputs( |$_ given real-tensors-if-given :$OrbitalCoefficients
                                              :$HartreeFockEnergy
                                              :$HoleEigenEnergies
                                              :$ParticleEigenEnergies)
}

#| MeanCorrelationHoleDepth
sub MeanCorrelationHoleDepth
  ( Str :$DoublesAmplitudes = <DoublesAmplitudes>
  , Str :$SinglesAmplitudes = ""
  , Str :$PPHHDelta = <PPHHDelta>
  , Str :$G = <G>
  ) of Algorithm is export
  {

  Algorithm.new:
    :name(&?ROUTINE.name)
    :signature(&?ROUTINE.signature)
    :inputs( (<DoublesAmplitudes>, $DoublesAmplitudes, <RealTensor>)
           , $SinglesAmplitudes ?? (<SinglesAmplitudes>, $SinglesAmplitudes, <RealTensor>) !! ()
           , (<PPHHDelta>, $PPHHDelta, <RealTensor>)
           )
    :outputs( (<G>, $G, <RealTensor>), )

}

#| OneBodyFromGaussian
sub OneBodyRotation
  ( Int :$No is required
  , Str :$OrbitalCoefficients is required = <OrbitalCoefficients>
  , Str :$Data is required
  , Str :$Out is required
  , Str :$hh
  , Str :$pp
  , Str :$hp
  , Str :$ph
  ) is export
  {
  Algorithm.new:
   :name(&?ROUTINE.name)
   :inputs( (<No>, $No)
          , |$_ given real-tensors-if-given :$OrbitalCoefficients
                                            :$Data
          )
  :outputs(|$_ given real-tensors-if-given :$hh :$pp :$hp :$ph)
}

#| CoulombIntegralsFromRotatedCoulombIntegrals
sub CoulombIntegralsFromRotatedCoulombIntegrals
  ( Str :$OrbitalCoefficients = <OrbitalCoefficients>
  , Str :$CoulombIntegrals = <CoulombIntegrals>
  , Str :$h = <HoleEigenEnergies>
  , Boolean :uhf($uhf) = 0
  , Boolean :chemistNotation($cn) = 1
  , *%ints
  ) of Algorithm is export
  {
  Algorithm.new:
    :name(&?ROUTINE.name)
    :signature(&?ROUTINE.signature)
    :inputs( (<OrbitalCoefficients>, $OrbitalCoefficients, <RealTensor>)
           , (<CoulombIntegrals>, $CoulombIntegrals, <RealTensor>)
           , (<HoleEigenEnergies>, $h, <RealTensor>)
           , $cn eq 1 ?? () !! (<chemistNotation>, $cn)
           , $uhf ?? (<Spins>, <Spins>, <RealTensor>) !! ()
           , $uhf ?? (<unrestricted>, 1) !! ()
           )
    :outputs(cints-hash-to-input-list %ints)
}

#| FiniteSizeCorrection
sub FiniteSizeCorrection
  ( Str :CoulombVertex($vertex) = <CoulombVertex>
  , Str :CoulombKernel($kernel) = <CoulombKernel>
  , Str :HoleEigenEnergies($h) = <HoleEigenEnergies>
  , Str :ParticleEigenEnergies($p) = <ParticleEigenEnergies>
  , Str :SinglesAmplitudes($tai) = ~_TENSOR_NAME_
  , Str :DoublesAmplitudes($tabij) = ~_TENSOR_NAME_
  , Str :StructureFactor($sofg) = <StructureFactor>
  ) of Algorithm is export
  {
  Algorithm.new:
    :name(&?ROUTINE.name)
    :signature(&?ROUTINE.signature)
    :inputs( (<HoleEigenEnergies>, $h, <RealTensor>)
           , (<ParticleEigenEnergies>, $p, <RealTensor>)
           , (<CoulombVertex>, $vertex, <RealTensor>)
           , (<CoulombKernel>, $kernel, <RealTensor>)
           , (<SinglesAmplitudes>, $tai, <RealTensor>)
           , (<DoublesAmplitudes>, $tabij, <RealTensor>)
           )
    :outputs( (<StructureFactor>, $sofg, <RealTensor>), )
}

sub TensorAntisymmetrizer(*%ints) of Algorithm is export {
  Algorithm.new: :name(&?ROUTINE.name)
                 :signature(&?ROUTINE.signature)
                 :inputs(cints-hash-to-input-list %ints)
}

sub TensorUnrestricter
  ( Str :$Data is required
  , Str :$Out = $Data
  ) of Algorithm is export
  {
  Algorithm.new:
    :name(&?ROUTINE.name)
    :signature(&?ROUTINE.signature)
    :inputs( real-tensors-if-given :$Data )
    :outputs( real-tensors-if-given :$Out )
}

sub UccsdAmplitudesFromCoulombIntegrals
  ( Boolean :$intermediates = 1
  , Boolean :$antisymmetrize = 1
  , Boolean :$unrestricted = 1
  , Boolean :onlyPPL($ppl) = 0
  , Str :$mixer where <LinearMixer> | <DiisMixer> = "LinearMixer"
  , Real :mixingRatio($mixRat) = -1
  , Int :MaxResidua($maxRes) = 0
  , Int :maxIterations($maxIter) = 0
  , Real :amplitudesConvergence($ampConv) = -1
  , Real :energyConvergence($enConv) = -1
  , Str :$HoleEigenEnergies = <HoleEigenEnergies>
  , Str :$ParticleEigenEnergies = <ParticleEigenEnergies>
  , Str :$initialDoublesAmplitudes = ""
  , Str :$initialSinglesAmplitudes = ""
  , Str :$HPFockMatrix = ""
  , Str :$PPFockMatrix = ""
  , Str :$HHFockMatrix = ""
  , Str :$UccsdDoublesAmplitudes = ""
  , Str :$UccsdSinglesAmplitudes = ""
  , Str :$UccsdEnergy = <UccsdEnergy>
  , Str :$PairEnergy = ""
  , Str :$HHHHCoulombIntegrals = <HHHHCoulombIntegrals>
  , Str :$HHHPCoulombIntegrals = <HHHPCoulombIntegrals>
  , Str :$HHPHCoulombIntegrals = <HHPHCoulombIntegrals>
  , Str :$HHPPCoulombIntegrals = <HHPPCoulombIntegrals>
  , Str :$HPHHCoulombIntegrals = <HPHHCoulombIntegrals>
  , Str :$HPHPCoulombIntegrals = <HPHPCoulombIntegrals>
  , Str :$HPPHCoulombIntegrals = <HPPHCoulombIntegrals>
  , Str :$HPPPCoulombIntegrals = <HPPPCoulombIntegrals>
#  , Str :$PHHHCoulombIntegrals = <PHHHCoulombIntegrals>
  , Str :$PHHPCoulombIntegrals = <PHHPCoulombIntegrals>
  , Str :$PHPHCoulombIntegrals = <PHPHCoulombIntegrals>
  , Str :$PHPPCoulombIntegrals = <PHPPCoulombIntegrals>
  , Str :$PPHHCoulombIntegrals = <PPHHCoulombIntegrals>
  , Str :$PPHPCoulombIntegrals = <PPHPCoulombIntegrals>
  , Str :$PPPHCoulombIntegrals = <PPPHCoulombIntegrals>
  , Str :$PPPPCoulombIntegrals = <PPPPCoulombIntegrals>
  , Boolean :Mp2Only($mp2) = 0
  ) of Algorithm is export
  {

  Algorithm.new:
    :name(&?ROUTINE.name)
    :signature(&?ROUTINE.signature)
    :inputs( :$intermediates.kv.list
           , :$antisymmetrize.kv.list
           , :$unrestricted.kv.list
           , $ppl ?? (<onlyPPL>, $ppl) !! ()
           , $mixer ?? (<mixer>, $mixer) !! ()
           , ($mixRat > 0) ?? (<mixingRatio>, $mixRat) !! ()
           , $maxRes ?? (<MaxResidua>, $maxRes) !! ()
           , $maxIter ?? (<maxIterations>, $maxIter) !! ()
           , ($ampConv > 0) ?? (<amplitudesConvergence> , $ampConv) !! ()
           , ($enConv > 0) ?? (<energyConvergence> , $enConv) !! ()
           , $mp2 ?? () !! (<HHHHCoulombIntegrals>, $HHHHCoulombIntegrals, <RealTensor>)
           , $mp2 ?? () !! (<HHHPCoulombIntegrals>, $HHHPCoulombIntegrals, <RealTensor>)
           , $mp2 ?? () !! (<HHPHCoulombIntegrals>, $HHPHCoulombIntegrals, <RealTensor>)
           , $mp2 ?? () !! (<HHPPCoulombIntegrals>, $HHPPCoulombIntegrals, <RealTensor>)
           , $mp2 ?? () !! (<HPHHCoulombIntegrals>, $HPHHCoulombIntegrals, <RealTensor>)
           , $mp2 ?? () !! (<HPHPCoulombIntegrals>, $HPHPCoulombIntegrals, <RealTensor>)
           , $mp2 ?? () !! (<HPPHCoulombIntegrals>, $HPPHCoulombIntegrals, <RealTensor>)
           , $mp2 ?? () !! (<HPPPCoulombIntegrals>, $HPPPCoulombIntegrals, <RealTensor>)
#           , $mp2 ?? () !! (<PHHHCoulombIntegrals>, $PHHHCoulombIntegrals, <RealTensor>)
           , $mp2 ?? () !! (<PHHPCoulombIntegrals>, $PHHPCoulombIntegrals, <RealTensor>)
           , $mp2 ?? () !! (<PHPHCoulombIntegrals>, $PHPHCoulombIntegrals, <RealTensor>)
           , $mp2 ?? () !! (<PHPPCoulombIntegrals>, $PHPPCoulombIntegrals, <RealTensor>)
           , $mp2 ?? () !! (<PPHPCoulombIntegrals>, $PPHPCoulombIntegrals, <RealTensor>)
           , $mp2 ?? () !! (<PPPHCoulombIntegrals>, $PPPHCoulombIntegrals, <RealTensor>)
           , $mp2 ?? () !! (<PPPPCoulombIntegrals>, $PPPPCoulombIntegrals, <RealTensor>)
           , |$_ given real-tensors-if-given
                 :$PPHHCoulombIntegrals
                 :$HPFockMatrix :$PPFockMatrix :$HHFockMatrix
                 :$HoleEigenEnergies
                 :$ParticleEigenEnergies
                 :$initialDoublesAmplitudes
                 :$initialSinglesAmplitudes
           )
    :outputs( |$_ given real-tensors-if-given :$UccsdEnergy
                                              :$UccsdDoublesAmplitudes
                                              :$UccsdSinglesAmplitudes
                                              :$PairEnergy
            )

}

sub UccsdtAmplitudesFromCoulombIntegrals
  ( Boolean :$intermediates = 1
  , Boolean :$antisymmetrize = 1
  , Boolean :$unrestricted = 1
  , Str :mixer($mixer) = ""
  , Real :mixingRatio($mixRat) = -1
  , Int :MaxResidua($maxRes) = 0
  , Int :maxIterations($maxIter) = 0
  , Real :amplitudesConvergence($ampConv) = -1
  , Real :energyConvergence($enConv) = -1
  , Str :$HoleEigenEnergies = <HoleEigenEnergies>
  , Str :$ParticleEigenEnergies = <ParticleEigenEnergies>
  , Str :$initialTriplesAmplitudes
  , Str :$initialDoublesAmplitudes
  , Str :$initialSinglesAmplitudes
  , Str :$HPFockMatrix = ""
  , Str :$PPFockMatrix = ""
  , Str :$HHFockMatrix = ""
  , Str :$UccsdTriplesAmplitudes = ""
  , Str :$UccsdDoublesAmplitudes = ""
  , Str :$UccsdSinglesAmplitudes = ""
  , Str :$UccsdEnergy = <UccsdEnergy>
  , Str :$HHHHCoulombIntegrals = <HHHHCoulombIntegrals>
  , Str :$HHHPCoulombIntegrals = <HHHPCoulombIntegrals>
  , Str :$HHPHCoulombIntegrals = <HHPHCoulombIntegrals>
  , Str :$HHPPCoulombIntegrals = <HHPPCoulombIntegrals>
  , Str :$HPHHCoulombIntegrals = <HPHHCoulombIntegrals>
  , Str :$HPHPCoulombIntegrals = <HPHPCoulombIntegrals>
  , Str :$HPPHCoulombIntegrals = <HPPHCoulombIntegrals>
  , Str :$HPPPCoulombIntegrals = <HPPPCoulombIntegrals>
  , Str :$PHHPCoulombIntegrals = <PHHPCoulombIntegrals>
  , Str :$PHPHCoulombIntegrals = <PHPHCoulombIntegrals>
  , Str :$PHPPCoulombIntegrals = <PHPPCoulombIntegrals>
  , Str :$PPHHCoulombIntegrals = <PPHHCoulombIntegrals>
  , Str :$PPHPCoulombIntegrals = <PPHPCoulombIntegrals>
  , Str :$PPPHCoulombIntegrals = <PPPHCoulombIntegrals>
  , Str :$PPPPCoulombIntegrals = <PPPPCoulombIntegrals>
  ) of Algorithm is export
  {

  Algorithm.new:
    :name(&?ROUTINE.name)
    :signature(&?ROUTINE.signature)
    :inputs( :$intermediates.kv.list
           , :$antisymmetrize.kv.list
           , :$unrestricted.kv.list
           , $mixer ?? (<Mixer>, $mixer) !! ()
           , ($mixRat > 0) ?? (<mixingRatio>, $mixRat) !! ()
           , $maxRes ?? (<MaxResidua>, $maxRes) !! ()
           , $maxIter ?? (<maxIterations>, $maxIter) !! ()
           , ($ampConv > 0) ?? (<amplitudesConvergence> , $ampConv) !! ()
           , ($enConv > 0) ?? (<energyConvergence> , $enConv) !! ()
           , |$_ given real-tensors-if-given
                 :$HHHHCoulombIntegrals :$HHHPCoulombIntegrals
                 :$HHPHCoulombIntegrals :$HHPPCoulombIntegrals
                 :$HPHHCoulombIntegrals :$HPHPCoulombIntegrals
                 :$HPPHCoulombIntegrals :$HPPPCoulombIntegrals
                 :$PHHPCoulombIntegrals :$PHPHCoulombIntegrals
                 :$PHPPCoulombIntegrals :$PPHHCoulombIntegrals
                 :$PPHPCoulombIntegrals :$PPPHCoulombIntegrals
                 :$PPPPCoulombIntegrals
                 :$HPFockMatrix :$PPFockMatrix :$HHFockMatrix
                 :$HoleEigenEnergies
                 :$ParticleEigenEnergies
                 :$initialTriplesAmplitudes
                 :$initialDoublesAmplitudes
                 :$initialSinglesAmplitudes
           )
    :outputs( |$_ given real-tensors-if-given :$UccsdEnergy
                                              :$UccsdTriplesAmplitudes
                                              :$UccsdDoublesAmplitudes
                                              :$UccsdSinglesAmplitudes
            )

}

grammar cc4s-range {
  token TOP { <single-range>* %% \, }
  token single-range { \s*\d+\s*[\-\s*\d+]?\s* }
}

sub CcsdEquationOfMotionDavidson
  ( Boolean :$complexVersion = 1
  , Str :$oneBodyRdmRange where {cc4s-range.parse($_)} = ""
  , Boolean :$printEigenvectorsDoubles where {cc4s-range.parse($_)} = 0
  , Str :$printEigenvectorsRange where {cc4s-range.parse($_)} = ""
  # Davidson solver
  , Real :$ediff = 0.00000001
  , Int :$maxBasisSize = 0
  , Boolean :$intermediates = 1
  , Int :$eigenstates is required = 2
  , Str :$refreshIterations where {cc4s-range.parse($_)} = ""
  , Boolean :$refreshOnMaxBasisSize = 0
  , Int :$maxIterations = 32
  , Int :$minIterations = 1
  # preconditioner
  , Boolean :$preconditionerRandom = 0
  , Real :$preconditionerRandomSigma = 0.01
  , Boolean :$preconditionerSpinFlip = 0
  # T amplitudes
  , Str :$SinglesAmplitudes is required
  , Str :$DoublesAmplitudes is required
  # Fock Matrix
  , Str :$ParticleEigenEnergies = <ParticleEigenEnergies>
  , Str :$HoleEigenEnergies = <HoleEigenEnergies>
  , Str :$HPFockMatrix = ""
  , Str :$HHFockMatrix = ""
  , Str :$PPFockMatrix = ""
  , Str :$HHHHCoulombIntegrals = <HHHHCoulombIntegrals>
  , Str :$PPPPCoulombIntegrals = <PPPPCoulombIntegrals>
  , Str :$HHHPCoulombIntegrals = <HHHPCoulombIntegrals>
  , Str :$HHPPCoulombIntegrals = <HHPPCoulombIntegrals>
  , Str :$HPHHCoulombIntegrals = <HPHHCoulombIntegrals>
  , Str :$HPHPCoulombIntegrals = <HPHPCoulombIntegrals>
  , Str :$HPPPCoulombIntegrals = <HPPPCoulombIntegrals>
  , Str :$PPHPCoulombIntegrals = <PPHPCoulombIntegrals>
  , Str :$PPPHCoulombIntegrals = <PPPHCoulombIntegrals>
  , Str :$PHPPCoulombIntegrals = <PHPPCoulombIntegrals>
  , Str :$PHPHCoulombIntegrals = <PHPHCoulombIntegrals>
  , Str :$HPPHCoulombIntegrals = <HPPHCoulombIntegrals>
  , Str :$HHPHCoulombIntegrals = <HHPHCoulombIntegrals>
  , Str :$PHHPCoulombIntegrals = <PHHPCoulombIntegrals>
  ) of Algorithm is export
  {
  Algorithm.new:
    :name(&?ROUTINE.name)
    :signature(&?ROUTINE.signature)
    :inputs( :$complexVersion.kv.list
           , :$oneBodyRdmRange.kv.list
           , :$printEigenvectorsDoubles.kv.list
           , :$printEigenvectorsRange.kv.list
           , :$ediff.kv.list
           , :$intermediates.kv.list
           , $maxBasisSize ?? :$maxBasisSize.kv.list !! ()
           , :$eigenstates.kv.list
           , :$refreshIterations.kv.list
           , :$refreshOnMaxBasisSize.kv.list
           , :$maxIterations.kv.list
           , :$minIterations.kv.list
           , :$preconditionerRandom.kv.list
           , :$preconditionerRandomSigma.kv.list
           , :$preconditionerSpinFlip.kv.list
           , |$_ given real-tensors-if-given
                 :$HHHHCoulombIntegrals :$PPPPCoulombIntegrals
                 :$HHHPCoulombIntegrals :$HHPPCoulombIntegrals
                 :$HPHHCoulombIntegrals :$HPHPCoulombIntegrals
                 :$HPPPCoulombIntegrals :$PPHPCoulombIntegrals
                 :$PPPHCoulombIntegrals :$PHPPCoulombIntegrals
                 :$PHPHCoulombIntegrals :$HPPHCoulombIntegrals
                 :$HHPHCoulombIntegrals :$PHHPCoulombIntegrals
                 :$HPFockMatrix :$PPFockMatrix :$HHFockMatrix
                 :$HoleEigenEnergies :$ParticleEigenEnergies
                 :$DoublesAmplitudes :$SinglesAmplitudes
           )
}


sub FcidumpReader
( Str :$file is required
, Int :$nelec = -1
, Str :$tt, Str :$tttt
, Str :$hh, Str :$pp, Str :$hp, Str :$ph
, Str :$hhhh, Str :$hhhp, Str :$hhph, Str :$hhpp
, Str :$hphh, Str :$hphp, Str :$hpph, Str :$hppp
, Str :$phhh, Str :$phhp, Str :$phph, Str :$phpp
, Str :$pphh, Str :$pphp, Str :$ppph, Str :$pppp
) of Algorithm is export
{
  Algorithm.new:
    :name(&?ROUTINE.name)
    :signature(&?ROUTINE.signature)
    :inputs( :$file.kv.list, :$nelec.kv.list )
    :outputs(|$_ given real-tensors-if-given :$tt :$tttt :$hh :$pp :$hp :$ph
                                             :$hhhh :$hhhp :$hhph :$hhpp
                                             :$hphh :$hphp :$hpph :$hppp
                                             :$phhh :$phhp :$phph :$phpp
                                             :$pphh :$pphp :$ppph :$pppp)
}

sub MoReader
( Str :$file = ~_MOVEC_FILE_
, Str :$xyzStructureFile = ~_PATH_TO_XYZ_FILE_
, Str :$basisFile = ""
, Str :$shellsFile = ""
, :$backend where * ∈ <nwchem psi4 turbomole> = "nwchem"
, Int :$No = -1
, Str :$OrbitalCoefficients = <OrbitalCoefficients>
, Str :$HoleEigenEnergies = <HoleEigenEnergies>
, Str :$ParticleEigenEnergies = <ParticleEigenEnergies>
) of Algorithm is export
{
  Algorithm.new:
    :name(&?ROUTINE.name)
    :signature(&?ROUTINE.signature)
    :inputs( :$file.kv.list
           , :$xyzStructureFile.kv.list
           , :$basisFile.kv.list
           , :$backend.kv.list
           , :$shellsFile.kv.list
           , (<No>, $No) )
    :outputs( |$_ given real-tensors-if-given :$OrbitalCoefficients
                                              :$HoleEigenEnergies
                                              :$ParticleEigenEnergies
                                              :$Spins
                                              )
}

sub TensorSlicer
( Str :$Data = ~_TENSOR_NAME_
, Str :$Out = ~_TENSOR_NAME_
, Str :$Begin
, Str :$End
) of Algorithm is export
{
  Algorithm.new:
    :name(&?ROUTINE.name)
    :signature(&?ROUTINE.signature)
    :inputs( (<Data>, $Data, <RealTensor>)
           , (<Out>, $Out, <RealTensor>)
           , (<Begin>, $Begin)
           , (<End>, $End)
           )

}

sub Mp2NaturalOrbitals
( Str :OrbitalCoefficients($coeff) = <OrbitalCoefficients>
, Str :HoleEigenEnergies($h) = <HoleEigenEnergies>
, Str :ParticleEigenEnergies($p) = <ParticleEigenEnergies>
, Str :PPHHCoulombIntegrals($pphh) = <PPHHCoulombIntegrals>
, Int :$Nfno
, Int :$FnoAlpha
, Int :$FnoBeta
, Boolean :uhf($uhf) = 0
, Rat :$occ = -1.0
, Str :RotatedOrbitals($orbs) = <RotatedOrbitals>
, Str :ParticleEigenEnergiesRediag($pRediag) = <ParticleEigenEnergiesRediag>
) of Algorithm is export
{
  Algorithm.new:
    :name(&?ROUTINE.name)
    :signature(&?ROUTINE.signature)
    :inputs( (<OrbitalCoefficients>, $coeff, <RealTensor>)
           , (<HoleEigenEnergies>, $h, <RealTensor>)
           , (<ParticleEigenEnergies>, $p, <RealTensor>)
           , (<PPHHCoulombIntegrals>, $pphh, <RealTensor>)
           , $Nfno ?? (<FnoNumber>, $Nfno) !! ()
           , ($occ > 0.0) ?? (<occupationThreshold>, $occ) !! ()
           , $uhf ?? (<Spins>, <Spins>, <RealTensor>) !! ()
           , $uhf ?? (<unrestricted>, 1) !! ()
           , $FnoAlpha ?? ( <FnoAlpha>, $FnoAlpha) !! ()
           , $FnoBeta  ?? ( <FnoBeta>, $FnoBeta) !! ()
           )
    :outputs( (<RotatedOrbitals>, $orbs, <RealTensor>)
            , (<ParticleEigenEnergiesRediag>, $pRediag, <RealTensor>)
            )


}

