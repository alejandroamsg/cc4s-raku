use Test;
use lib 'lib';
use Cc4s;

subtest {

  my @a = ( &TensorReader
          , &TensorWriter
          , (&{TensorSum :A<A> :B<B> :ResultIndex<sd> :AIndex<ij> :BIndex<ij>})
          , &ComplexTensorWriter
          , &ComplexTensorReader
          , &CoulombVertexReader
          , &TensorAntisymmetrizer
        # Basic algorithms
          , &CcsdEnergyFromCoulombIntegrals
          , &CoulombIntegralsFromVertex
          , &Mp2EnergyFromCoulombIntegrals
          , &CoulombIntegralsFromRotatedCoulombIntegrals
          , &UccsdAmplitudesFromCoulombIntegrals
          , &UccsdtAmplitudesFromCoulombIntegrals
        # Compatibility
          , (&{ FcidumpReader :file<FCIDUMP> })
        # Hartree fock related
          , &HartreeFockFromGaussian
          , &FockMatrixFromCoulombIntegrals
          , (&{ HartreeFockFromCoulombIntegrals :3No })
          , &CoulombIntegralsFromGaussian
          , (&{OneBodyFromGaussian :Out<Blah> :kernel<nuclear>})
          , &MeanCorrelationHoleDepth
          );

  for @a {
    given $_.() { ok ~$_, "creating {$_.name}"; }
  }

}, <Creation of algorithms>;
